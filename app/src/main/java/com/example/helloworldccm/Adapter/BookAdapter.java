package com.example.helloworldccm.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helloworldccm.Holder.BookHolder;
import com.example.helloworldccm.Interfaces.IActionLivre;
import com.example.helloworldccm.Model.Book;
import com.example.helloworldccm.R;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookHolder> {

    private List<Book> books;

    private IActionLivre monActionLivre;

    public BookAdapter(List<Book> books, IActionLivre actionLivre) {
        this.books = books;
        this.monActionLivre = actionLivre;
    }

    @NonNull
    @Override
    public BookHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View myView = inflater.inflate(R.layout.layout_line_list_authot, parent, false);

        return new BookHolder(myView, this.monActionLivre);
    }

    @Override
    public void onBindViewHolder(@NonNull BookHolder holder, int position) {
        //mise a jour de la ligne avec le position'eme livre
        holder.majLine(this.books.get(position));
    }

    @Override
    public int getItemCount() {
         return this.books.size();
    }
}
