package com.example.helloworldccm.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helloworldccm.Model.Pojo;
import com.example.helloworldccm.Holder.PojoViewHolder;
import com.example.helloworldccm.R;

import java.util.List;

public class PojoAdapter extends RecyclerView.Adapter<PojoViewHolder> {

    private List<Pojo> lesPojos;

    /**
     *
     * @param pojos
     */
    public PojoAdapter(List<Pojo> pojos) {
        this.lesPojos = pojos;
    }

    @NonNull
    @Override
    public PojoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Le layout inflateur est utilisé pour exploiter les fichiers XML pour fabriquer les IHM à partir des XML
        LayoutInflater lyi = LayoutInflater.from(parent.getContext());

        View maVue = lyi.inflate(R.layout.layout_recycler_line, parent, false);

        return new PojoViewHolder(maVue);
    }

    // But de la méthode : visiualiser le position'eme element de la liste.
    @Override
    public void onBindViewHolder(@NonNull PojoViewHolder holder, int position) {
        holder.majPojo(this.lesPojos.get(position));
    }

    //But de la méthode : retourner le nombre d'item
    @Override
    public int getItemCount() {
        return this.lesPojos.size();
    }
}
