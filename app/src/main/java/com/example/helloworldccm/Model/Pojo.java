package com.example.helloworldccm.Model;

import java.util.Date;

public class Pojo {
    private static int compteur = 0;

    private int numero;

    private String nom;

    private Date date;

    /**
     * Constructeur
     * @param nom
     * @param date
     */
    public Pojo(String nom, Date date) {
        this.numero = compteur++;
        this.nom = nom;
        this.date = date;
    }

    /**
     * Constructeur par défault
     */
    public Pojo() {
        this("default"+ compteur, new Date());
    }

    public static int getCompteur() {
        return compteur;
    }

    public int getNumero() {
        return numero;
    }

    public String getNom() {
        return nom;
    }

    public Date getDate() {
        return date;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Pojo{" +
                "numero=" + numero +
                ", nom='" + nom + '\'' +
                ", date=" + date +
                '}';
    }
}
