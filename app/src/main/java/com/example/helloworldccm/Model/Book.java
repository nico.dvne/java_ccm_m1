package com.example.helloworldccm.Model;

public class Book {

    private String title;

    private String name;

    public Book(String name, String title) {
        this.title = title;
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
