package com.example.helloworldccm.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.helloworldccm.Activity.TroisiemeActivite;
import com.example.helloworldccm.R;

public class QuatriemeActivite extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_quatrieme_activite);
    }

    public void onClickQuitter(View view) {
        Bundle fourthBundle = new Bundle();
        fourthBundle.putString("fourth_result", "I come from fourth activity");

        setResult(RESULT_OK, new Intent().putExtras(fourthBundle));
        finish();
    }
}