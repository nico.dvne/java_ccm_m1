package com.example.helloworldccm.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.helloworldccm.Adapter.BookAdapter;
import com.example.helloworldccm.Interfaces.IActionLivre;
import com.example.helloworldccm.Model.Book;
import com.example.helloworldccm.R;

import java.util.ArrayList;
import java.util.List;

public class BookVisualisationActivity extends AppCompatActivity {

    private ArrayList<String> lesLivresArrayList;
    private List<Book> lesLivres;

    private RecyclerView recyclerView;

    private IActionLivre actionLivre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_book_visualisation);

        this.recyclerView = findViewById(R.id.id_visualisation_recycler_view);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Intent activityIntent = getIntent();
        Bundle receiptBundle = activityIntent.getExtras();
        this.lesLivresArrayList = receiptBundle.getStringArrayList("the_books");
        this.lesLivres = new ArrayList<Book>();

        for (int i = 0; i < this.lesLivresArrayList.size() - 2; i+=2) {
            lesLivres.add(
                    new Book(lesLivresArrayList.get(i), lesLivresArrayList.get(i + 1))
            );
        }

        this.actionLivre = new IActionLivre() {
            @Override
            public void yaEuUnClic(int position) {
                Toast.makeText(BookVisualisationActivity.this, "Le livre a la position " + position + " a ete selectionné", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void yaEuUnClicLong(int position) {
                Toast.makeText(BookVisualisationActivity.this, "Clic long, position :" + position, Toast.LENGTH_SHORT).show();
            }
        };

        this.recyclerView.setAdapter(new BookAdapter(lesLivres, this.actionLivre));
    }

    public void onClickExit(View view) {
        finish();
    }
}