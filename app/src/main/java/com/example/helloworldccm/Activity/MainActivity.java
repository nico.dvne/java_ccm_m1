package com.example.helloworldccm.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.helloworldccm.R;

public class MainActivity extends AppCompatActivity {


    private Button secondBtn;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.secondBtn = findViewById(R.id.btn_exit_seconde_facon);
        this.secondBtn.setOnClickListener(view -> leaveActivity());
    }

    /**
     *
     * @param view
     */
    public void onClickPremiereFacon(View view) {
        leaveActivity();
    }


    private void leaveActivity()
    {
        finish();
    }

    public void onClickEasiestWay(View view) {
        Intent intentRedirect = new Intent(this, SecondeActivite.class);
        Bundle monBundle = new Bundle();
        monBundle.putString("un_nom", "Valeur un peu nulle");
        monBundle.putInt("une_annee", 2022);
        monBundle.putInt("un_mois", 1);
        monBundle.putInt("un_jour", 26);

        intentRedirect.putExtras(monBundle);

        startActivity(intentRedirect);
    }

    public void onClickCreateAuthor(View view) {
        startActivity(new Intent(this, AuthorActivity.class));
    }

    public void onClickLaunchWithReturnDatas(View view) {
        Intent thirdIntent = new Intent(this, TroisiemeActivite.class);
        startActivity(thirdIntent);
    }
}