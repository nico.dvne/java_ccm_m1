package com.example.helloworldccm.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.helloworldccm.R;

public class TroisiemeActivite extends AppCompatActivity {

    public static final int FOURTH_REQUEST_CODE = 2;
    public static final int FIFTH_REQUEST_CODE = 3;

    private TextView results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_troisieme_activite);
        results = findViewById(R.id.txt_third_returns);
    }

    public void onClickGoToFourth(View view) {
        Intent intentFourth = new Intent(this, QuatriemeActivite.class);

        startActivityForResult(intentFourth, FOURTH_REQUEST_CODE);
    }

    public void onClickGoToFifth(View view) {
        Intent intentFifth = new Intent(this, CinquiemeActivite.class);

        startActivityForResult(intentFifth, FIFTH_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        this.results.setText("Retour KO");

        switch (requestCode) {
            case FOURTH_REQUEST_CODE:
                this.results.setText(this.results.getText() + " from fourth");
                if (RESULT_OK == resultCode) {
                    this.results.setText(data.getExtras().getString("fourth_result"));
                }

                break;
            case FIFTH_REQUEST_CODE:
                this.results.setText(this.results.getText() + " from fifth");
                if (RESULT_OK == resultCode) {
                    this.results.setText(data.getExtras().getString("fifth_result"));
                }
                break;
        }
    }
}