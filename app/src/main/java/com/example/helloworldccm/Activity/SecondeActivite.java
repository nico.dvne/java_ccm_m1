package com.example.helloworldccm.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.helloworldccm.Adapter.PojoAdapter;
import com.example.helloworldccm.Model.Pojo;
import com.example.helloworldccm.R;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SecondeActivite extends AppCompatActivity {

    private List<Pojo> pojos;
    private RecyclerView monRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seconde_activite);
        this.monRecyclerView = findViewById(R.id.id_recycler_view);

        //Recupération de l'intent de l'activité puis, de son bundle associé
        Bundle bundle = getIntent().getExtras();
        pojos = this.initPojos();
        //Ajout du pojo récupéré via le bundle
        pojos.add(new Pojo(
                bundle.getString("un_nom"),
                new Date(
                        bundle.getInt("une_annee"),
                        bundle.getInt("un_mois"),
                        bundle.getInt("un_jour")
                )
        ));

        // LinearLayoutManager -> le type de layout pour les lignes du recyclerView
        this.monRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        PojoAdapter pojoAdapter = new PojoAdapter(pojos);
        this.monRecyclerView.setAdapter(pojoAdapter);
    }

    public void onClickQuitterActivite(View view) {
        finish();
    }

    private List<Pojo> initPojos() {
        Log.d("NICO", "Init pojos method");

        Toast.makeText(this, "Init pojos method", Toast.LENGTH_LONG).show();

        return this.pojos = IntStream.range(0, 100)
                .mapToObj(i -> {return new Pojo("Nom" + i, new Date());})
                .collect(Collectors.toList());
    }
}