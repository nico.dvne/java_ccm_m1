package com.example.helloworldccm.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.helloworldccm.Adapter.BookAdapter;
import com.example.helloworldccm.Model.Book;
import com.example.helloworldccm.R;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AuthorActivity extends AppCompatActivity {

    private EditText edt_title;
    private EditText edt_name;

    private List<Book> books;

    private BookAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_author);
        this.edt_name = findViewById(R.id.edt_author_name);
        this.edt_title = findViewById(R.id.edt_author_title);
        this.books = new LinkedList<>();
    }

    public void onClickExit(View view) {
        finish();
    }

    public void onClickAdd(View view) {
        String bookName = this.edt_name.getText().toString();
        String bookTitle = this.edt_title.getText().toString();
        this.books.add(
                new Book(bookTitle, bookName)
        );

        Log.e("NICOLAS", this.books.toString());
    }

    public void onClickVisualiser(View view) {
        Intent showIntent = new Intent(this, BookVisualisationActivity.class);
        Bundle bundle = new Bundle();

        ArrayList<String> bufferBooks = new ArrayList();

        for (Book book: this.books) {
            bufferBooks.add(book.getName());
            bufferBooks.add(book.getTitle());
        }
        bundle.putStringArrayList("the_books", bufferBooks);

        showIntent.putExtras(bundle);
        startActivity(showIntent);
    }
}