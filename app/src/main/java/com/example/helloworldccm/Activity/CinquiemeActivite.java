package com.example.helloworldccm.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.helloworldccm.R;

public class CinquiemeActivite extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_cinquieme_activite);
    }

    public void onClickExit(View view) {
        Bundle fifthBundle = new Bundle();
        fifthBundle.putString("fifth_result", "I come from fifth activity");

        setResult(RESULT_CANCELED, new Intent().putExtras(fifthBundle));

        finish();
    }
}