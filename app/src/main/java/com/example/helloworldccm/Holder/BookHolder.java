package com.example.helloworldccm.Holder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helloworldccm.Interfaces.IActionLivre;
import com.example.helloworldccm.Model.Book;
import com.example.helloworldccm.R;

import java.util.Objects;

public class BookHolder extends RecyclerView.ViewHolder{

    private TextView txt_name;

    private TextView txt_title;

    public BookHolder(@NonNull View itemView, IActionLivre monActionLivre) {
        super(itemView);

        this.txt_name = itemView.findViewById(R.id.txt_line_author_name);
        this.txt_title = itemView.findViewById(R.id.txt_line_author_title);

        itemView.setOnClickListener(view -> monActionLivre.yaEuUnClic(getAdapterPosition()));
        itemView.setOnLongClickListener(view -> {
                    monActionLivre.yaEuUnClicLong(getAdapterPosition());
                    return false;
        });
    }

    public void majLine(Book aBook) {
        if (Objects.nonNull(aBook)) {
            this.txt_title.setText(aBook.getTitle());
            this.txt_name.setText(aBook.getName());
        }
    }
}
