package com.example.helloworldccm.Holder;


import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.helloworldccm.Model.Pojo;
import com.example.helloworldccm.R;

import java.util.Objects;

// Le viewHolder est un 'transporteur de ligne'
public class PojoViewHolder extends RecyclerView.ViewHolder {

    private TextView txtview_name;

    private TextView txtview_date;

    /**
     *
     * @param itemView
     */
    public PojoViewHolder(@NonNull View itemView) {
        super(itemView);

        this.txtview_name = itemView.findViewById(R.id.id_textview_ligne_nom);
        this.txtview_date = itemView.findViewById(R.id.id_textview_ligne_date);
    }

    /**
     *
     * @param pojo
     */
    public void majPojo(Pojo pojo) {
        if (Objects.nonNull(pojo)) {
            this.txtview_name.setText(pojo.getNom());
            this.txtview_date.setText(pojo.getDate().toString());
        }
    }
}
